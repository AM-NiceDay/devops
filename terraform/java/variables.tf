variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "gitlab_user" {
  default = ""
}

variable "gitlab_password" {
  default = ""
}
