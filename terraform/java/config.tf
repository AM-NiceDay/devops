provider "aws" {
  region     = "eu-central-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_key_pair" "terraform_key" {
  key_name   = "terraform_key"
  public_key = file("~/.ssh/terraform_rsa.pub")
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "builder" {
  key_name        = aws_key_pair.terraform_key.key_name
  ami             = "ami-0d359437d1756caa8"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.allow_ssh.name]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/terraform_rsa")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "cd ~/",
      "git clone https://github.com/boxfuse/boxfuse-sample-java-war-hello.git boxfuse"
    ]
  }

  provisioner "file" {
    source      = "Dockerfile"
    destination = "/home/ubuntu/boxfuse/Dockerfile"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo apt -y install docker.io",
      "cd ~/boxfuse",
      "sudo docker login registry.gitlab.com -u ${var.gitlab_user} -p ${var.gitlab_password}",
      "sudo docker build -t boxfuse .",
      "sudo docker tag boxfuse registry.gitlab.com/am-niceday/devops/boxfuse:latest",
      "sudo docker push registry.gitlab.com/am-niceday/devops/boxfuse:latest"
    ]
  }
}

resource "aws_instance" "production" {
  key_name        = aws_key_pair.terraform_key.key_name
  ami             = "ami-0d359437d1756caa8"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.allow_ssh.name, aws_security_group.allow_http.name]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/terraform_rsa")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "cd ~/",
      "mkdir boxfuse"
    ]
  }

  provisioner "file" {
    source      = "docker-compose.yml"
    destination = "/home/ubuntu/boxfuse/docker-compose.yml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo apt -y install docker.io",
      "sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "cd ~/boxfuse",
      "sudo docker login registry.gitlab.com -u ${var.gitlab_user} -p ${var.gitlab_password}",
      "sudo docker-compose up -d"
    ]
  }

  depends_on = [
    aws_instance.builder,
  ]
}
