# Create AWS instance and provision Nginx

## Before running a terraform config you have to create an ssh key and login into aws cli

```
$ ssh-keygen -t rsa
  ~/.ssh/terraform_rsa

$ chmod 400 ~/.ssh/terraform_rsa
```
