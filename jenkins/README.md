# Настройка Jenkins - Nexus

## Настройка Nexus

- Загрузить на сервер docker-compose файл:

```
// nexus/docker-compose.yml

version: "3.3"
services:
  nexus:
    image: sonatype/nexus3
    ports:
      - "8081:8081"
      - "8123:8123"
    volumes:
      - type: volume
        source: nexus-data
        target: /nexus-data

volumes:
  nexus-data:
```

- Установить docker-compose – `apt update && apt install docker-compose`
- Запустить nexus командой `docker-compose up -d`
- После запуска Nexus создать пользователя admin
- Создать новый Docker Hosted репозиторий с разрешением на анонимный docker pull
- Активировать Docker Bearer Token Realm

## Настройка Jenkins

### Запуск Jenkins

- Зайти на сервер
- Установить docker-compose – `apt update && apt install docker.io`
- Запустить jenkins контейнер командой `docker run -p 8080:8080 -p 50000:50000 -d jenkins/jenkins:lts`
- Открыть jenkins в браузере на порту 8080
- Взять код из логов контенера jenkins, запустив команду `docker logs {container_id}`
- Активировать Jenkins и создать пользователя администратора

### Настройка job-ов

- Установить плагины ssh и ssh-agent
- Настроить две пары credentials для доступа к удаленным серверам: первый - `Builder`, второй - Prod
- Создать два удаленных ssh соединения с использованием credentials

#### Builder job

- Создать `Builder` job
- Добавить параметр `tag` со значением по умолчанию `latest`
- Добавить Build step `Execute shell script on remote host using ssh` и выбрать соединение с сервером `Builder`
- Добавить ssh script:

```
apt update
apt install -y docker.io

if [ ! -e /etc/docker/daemon.json ]; then
  echo "{ \"insecure-registries\" : [\"185.255.79.18:8123\"] }" > /etc/docker/daemon.json
  service docker restart
fi

docker login -u admin -p qwerty 185.255.79.18:8123

rm -rf webapp
git clone https://github.com/docker-training/webapp.git
cd webapp

docker build -t webapp .
docker tag webapp:$tag 185.255.79.18:8123/webapp

docker push 185.255.79.18:8123/webapp
```

#### Prod job

- Создать `Prod` job
- Добавить параметр `tag` со значением по умолчанию `latest`
- Добавить Build step `Execute shell script on remote host using ssh` и выбрать соединение с сервером `Prod`
- Добавить ssh script:

```
apt update
apt install -y docker.io

if [ ! -e /etc/docker/daemon.json ]; then
  echo "{ \"insecure-registries\" : [\"185.255.79.18:8123\"] }" > /etc/docker/daemon.json
  service docker restart
fi

docker stop $(docker ps -aq)
docker run -p 5000:5000 -d 185.255.79.18:8123/webapp
```
